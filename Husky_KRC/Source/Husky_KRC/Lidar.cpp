/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

#include "Lidar.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Materials/Material.h"
#include "EngineUtils.h"
#include "Engine/StaticMeshActor.h"

// Sets default values
ALidar::ALidar()
  : scan_start_angle_(-179), scan_end_angle_(179), vertical_FOV_(30.0), distance_(30.0),
    num_channels_(16), vert_angle_offset_(0.0), h_angle_per_sample_(0.1), noise_std_dev_(0.03)
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = false;

  // Creates parent LiDAR component
  USceneComponent* lidarComponent = CreateDefaultSubobject<USceneComponent>(TEXT("LiDAR"));
  RootComponent = lidarComponent;

  // Creates the cylinder of the actual LiDAR sensors
  UStaticMeshComponent* cylinder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cylinder"));
  cylinder->SetupAttachment(RootComponent);
  // Setup Static Mesh
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    cylinderMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cylinder.Cylinder'"));
  if (cylinderMesh.Succeeded())
  {
    cylinder->SetStaticMesh(cylinderMesh.Object);
    // Turn off collision, otherwise the Husky becomes Airborne!
    cylinder->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    cylinder->SetEnableGravity(false);
    //cylinder->SetSimulatePhysics(false);
    cylinder->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
    cylinder->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
  }
  // Setup Material for Static Mesh
  static ConstructorHelpers::FObjectFinder<UMaterial>
    basicMaterial(TEXT("Material'/Engine/BasicShapes/BasicShapeMaterial.BasicShapeMaterial'"));
  if (basicMaterial.Succeeded())
  {
    cylinder->SetMaterial(0, basicMaterial.Object);
  }
  cylinder->SetRelativeScale3D(FVector(0.25, 0.25, 0.25));
  
  // Creates the start trace arrow
  start_trace_ = CreateDefaultSubobject<UArrowComponent>(TEXT("Start Trace Location"));
  start_trace_->SetupAttachment(RootComponent);
  start_trace_->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
  start_trace_->SetHiddenInGame(true);

  // Creates the end trace arrow
  end_trace_ = CreateDefaultSubobject<UArrowComponent>(TEXT("End Trace Location"));
  end_trace_->SetupAttachment(RootComponent);
  end_trace_->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
  end_trace_->SetHiddenInGame(true);

  // RNGs for noise generation
  gen_x_ = std::ranlux24(rd_());
  gen_y_ = std::ranlux24(rd_());
  gen_z_ = std::ranlux24(rd_());
}

// Called when the game starts or when spawned
void ALidar::BeginPlay()
{
  Super::BeginPlay();
  
  // Set the starting rotation
  starting_rotation_ = RootComponent->RelativeRotation;
  
  // Just a sanity check....
  if(scan_start_angle_ > scan_end_angle_)
  {
    float temp = scan_start_angle_;
    scan_start_angle_ = scan_end_angle_;
    scan_end_angle_ = temp;
  }
  
  starting_rotation_.Yaw = scan_start_angle_;

  // Calculate the End Arrow offset
  FVector endTraceLocation = end_trace_->RelativeLocation;
  endTraceLocation.X += (distance_ * 100);
  end_trace_->AddLocalOffset(endTraceLocation, false, nullptr, ETeleportType::TeleportPhysics);

  // --- CALCULATING CHANNEL HEIGHT OFFSETS ---
  float channelAngle = vertical_FOV_ / (num_channels_ - 1);
  float _vertAngle = channelAngle / 2.0;

  // Channels above the center
  for (int i = 1; i <= num_channels_ / 2; i++)
  {
    height_offsets_.Add(calculateHeight(i * channelAngle - _vertAngle + vert_angle_offset_));
  }

  // Center channel
  height_offsets_.Add(calculateHeight(_vertAngle * -1 + vert_angle_offset_));

  // Channels below the center
  for (int i = 1; i <= num_channels_ / 2 - 1; i++)
  {
    height_offsets_.Add(calculateHeight(i * channelAngle * -1 - _vertAngle + vert_angle_offset_));
  }

  // Distribution for noise
  // Placed here so we can get the value from the editor, if it differs from the default
  noise_dist_ = std::normal_distribution<float>(0.0f, noise_std_dev_);

  // Add the parent of the LIDAR to the ignore list so the Line Traces ignore this actor
  to_ignore_.Add(RootComponent->GetAttachParent()->GetOwner());
}

// We only need this function if we are in the editor
#if WITH_EDITOR
// Make sure we reset the distribution if we change the stddev
void ALidar::PostEditChangeProperty(struct FPropertyChangedEvent &Event)
{
  if (Event.GetPropertyName() == GET_MEMBER_NAME_CHECKED(ALidar, noise_std_dev_))
  {
    noise_dist_ = std::normal_distribution<float>(0.0f, noise_std_dev_);
  }
  else if (Event.GetPropertyName() == GET_MEMBER_NAME_CHECKED(ALidar, scan_start_angle_) && 
    scan_start_angle_ > scan_end_angle_)
  {
    scan_start_angle_ = scan_end_angle_ - 1.0F;
    if (scan_start_angle_ < -179.9F)
    {
      scan_start_angle_ = -179.9F;
    }
  }
  else if (Event.GetPropertyName() == GET_MEMBER_NAME_CHECKED(ALidar, scan_start_angle_) &&
    scan_end_angle_ < scan_start_angle_)
  {
    scan_end_angle_ = scan_start_angle_ + 1.0F;
    if (scan_end_angle_ > 179.9F)
    {
      scan_end_angle_ = 179.9F;
    }
  }
  Super::PostEditChangeProperty(Event);
}
#endif // WITH_EDITOR

// Called every frame
void ALidar::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

void ALidar::doFullCast(TArray<float> &points, TArray<float> &perfect_points, FTransform world_to_map_tf,
  FTransform &map_to_current_tf)
{
  // Setup for our Ray Casts
  points.Empty(); // Start by making sure the points array is empty to start. (It should be, but can't hurt)
  perfect_points.Empty();
  FRotator each_rot(0.0, h_angle_per_sample_, 0.0); // Constant rotator for rotating our LiDAR
  FHitResult result; // The result of our Line Trace will be stored here

  // Move the points returned from LineTrace to be in our LIDAR frame
  FTransform temp_T = RootComponent->GetAttachParent()->GetAttachParent()->GetComponentTransform();
  FTransform base_link_tf =
    RootComponent->GetAttachParent()->GetAttachParent()->GetAttachParent()->GetComponentTransform();

  // Create a transform from the center of the scan to the origin
  FQuat temp = FQuat(FRotator(temp_T.GetRotation().Rotator().Pitch, -temp_T.GetRotation().Rotator().Yaw,
    -temp_T.GetRotation().Rotator().Roll));
  FTransform base_link_to_vlp = FTransform(temp);
  map_to_current_tf = base_link_to_vlp * FTransform(world_to_map_tf.TransformPosition(temp_T.GetTranslation()));
  
  RootComponent->AddLocalRotation(each_rot); // Do the first one manually
  for (; RootComponent->RelativeRotation.Yaw < scan_end_angle_; RootComponent->AddLocalRotation(each_rot))
  {
    // Make the Ray Casts
    for (float offset : height_offsets_)
    {
      // Make the cast itself!
      UKismetSystemLibrary::LineTraceSingle(this, start_trace_->GetComponentLocation(),
        end_trace_->GetComponentLocation() + FVector(0.0, 0.0, offset),
        UEngineTypes::ConvertToTraceType(ECC_Visibility), false, to_ignore_, EDrawDebugTrace::Type::None, result,
        true);

      if (!result.Location.Equals(FVector::ZeroVector))
      {
        FVector noise(noise_dist_(gen_x_), noise_dist_(gen_y_), noise_dist_(gen_z_));
        
        FVector local_loc = temp_T.InverseTransformPosition(result.Location) / 100.0F + noise;

        points.Add(local_loc.X);
        points.Add(-local_loc.Y);
        points.Add(local_loc.Z);
        
        // Now output point cloud in the map frame to produce a perfect point cloud.
        FVector ideal_points = world_to_map_tf.TransformPosition(result.Location) / 100.0F + noise;
        
        perfect_points.Add(ideal_points.X);
        perfect_points.Add(-ideal_points.Y);
        perfect_points.Add(ideal_points.Z);
      }
    }
  }

  // A full sweep has been done
  RootComponent->SetRelativeRotation(starting_rotation_);
}

// Small performance boost in startup when inlined
FORCEINLINE float ALidar::calculateHeight(float angle)
{
  // DegTan is inlined to FMath::Tan with a degrees to radians conversion
  return UKismetMathLibrary::DegTan(angle) * distance_ * 100 ; // UU (cm) to m
}

