/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

#include "Husky.h"
#include "ConstructorHelpers.h"
#include "ROSIntegration/Classes/ROSIntegrationGameInstance.h"
#include "ROSIntegration/Public/rosgraph_msgs/Clock.h"
#include "ROSIntegration/Public/sensor_msgs/PointCloud2.h"
#include "ROSIntegration/Public/geometry_msgs/Twist.h"
#include "ROSIntegration/Public/nav_msgs/Odometry.h"
#include "ROSIntegration/Public/sensor_msgs/Imu.h"
#include "ROSIntegration/Public/tf2_msgs/TFMessage.h"

// AHUSKY
// Sets default values
AHusky::AHusky()
  : max_velocity_(5.0F), wheel_radius_(0.1651F), damping_(10.0F), drive_update_freq_(1.0F), engine_torque_(0.0F),
    rpm_(50.0F), current_gain_(20.0F), is_accelerating_(false), rpm_limit_(3000.0F), accel_factor_(100.0F),
    decel_factor_(100.0F), old_time_(0.0F), old_time_2_(0.0F), pos_std_dev_(0.03F), rot_std_dev_(0.03F),
    lidar_class_(ALidar::StaticClass()), linear_(ForceInit), angular_(ForceInit), linear_V_(ForceInit),
    angular_V_(ForceInit), imu_pub_topic_name_(TEXT("/imu/data")), pt_cld2_pub_name_(TEXT("/velodyne_points")),
    gps_pub_name_(TEXT("/odometry/gps")), wheel_pub_name_(TEXT("/husky_velocity_controller/odom")),
    twist_sub_topic_name_(TEXT("/cmd_vel")), tf2_pub_topic_name_(TEXT("/tf")), imu_pub_rate_(10.0F),
    gps_pub_rate_(1.0F)
{
  // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
  SetTickGroup(ETickingGroup::TG_PostPhysics);

  // The various Static Meshes that we use
  // NOTE: We may potentially want to load these Static Meshes in the editor and not in code.
  // Since we're not deploying as a game, it might be okay, but the Unreal Docs recommend not doing it this way.
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    base_link_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/base_link.base_link'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    top_chassis_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/top_chassis.top_chassis'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    wheel_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/wheel.wheel'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    bumper_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/bumper.bumper'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    top_plate_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/top_plate.top_plate'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    top_frame_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/top_frame.top_frame'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    vn100_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/vn100.vn100'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    vlp_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/vlp.vlp'"));
  static ConstructorHelpers::FObjectFinder<UStaticMesh>
    emlid_mesh(TEXT("StaticMesh'/Game/Husky/Meshes/emlid.emlid'"));

  // --- THE COMPONENTS ---
  // Base Link will be our RootComponent
  // Call it RootComponent to avoid some bugs during construction.
  // UE automatically changes the name of the RootComponent later to RootComponent
  UStaticMeshComponent* base_link = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
  RootComponent = base_link;
  base_link->SetSimulatePhysics(true);
  base_link->SetCollisionProfileName(TEXT("BlockAll"));
  if (base_link_mesh.Succeeded())
  {
    base_link->SetStaticMesh(base_link_mesh.Object);
  }

  // Spring Arm with External Camera
  spring_arm_ = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Spring Arm"));
  spring_arm_->SetupAttachment(RootComponent);
  spring_arm_->SetRelativeLocation(FVector(0.0F, 0.0F, 50.0F));
  spring_arm_->bUsePawnControlRotation = true;
  external_cam_ = CreateDefaultSubobject<UCameraComponent>(TEXT("External Camera"));
  external_cam_->SetupAttachment(spring_arm_);
  external_cam_->bUsePawnControlRotation = true;

  // Top Chassis
  top_chassis_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Top Chassis"));
  top_chassis_->SetupAttachment(RootComponent);
  top_chassis_->SetEnableGravity(false);
  if (top_chassis_mesh.Succeeded())
  {
    top_chassis_->SetStaticMesh(top_chassis_mesh.Object);
  }

  // Wheels!
  // These are all the same except for positions. Hence why we use a method.
  setupWheel(&wheel_FR_, &wheel_phys_FR_, TEXT("Wheel FR"), TEXT("Wheel Physics FR"), FVector( 25.6F,  28.54F, 3.282F),
    wheel_mesh.Object);
  setupWheel(&wheel_FL_, &wheel_phys_FL_, TEXT("Wheel FL"), TEXT("Wheel Physics FL"), FVector( 25.6F, -28.54F, 3.282F),
    wheel_mesh.Object);
  setupWheel(&wheel_RR_, &wheel_phys_RR_, TEXT("Wheel RR"), TEXT("Wheel Physics RR"), FVector(-25.6F,  28.54F, 3.282F),
    wheel_mesh.Object);
  setupWheel(&wheel_RL_, &wheel_phys_RL_, TEXT("Wheel RL"), TEXT("Wheel Physics RL"), FVector(-25.6F, -28.54F, 3.282F),
    wheel_mesh.Object);

  // Bumpers
  // There isn't as much here, so I'm just going to duplicate code
  front_bumper_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Front Bumper"));
  front_bumper_->SetupAttachment(RootComponent);
  front_bumper_->SetRelativeLocation(FVector(48.0F, 0.0F, 9.1F));
  front_bumper_->SetEnableGravity(false);
  if (bumper_mesh.Succeeded())
  {
    front_bumper_->SetStaticMesh(bumper_mesh.Object);
  }
  rear_bumper_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rear Bumper"));
  rear_bumper_->SetupAttachment(RootComponent);
  rear_bumper_->SetRelativeLocationAndRotation(FVector(-48.0F, 0.0F, 9.1F), FRotator(180.0F, 0.0F, 0.0F));
  rear_bumper_->SetEnableGravity(false);
  if (bumper_mesh.Succeeded())
  {
    rear_bumper_->SetStaticMesh(bumper_mesh.Object);
  }

  // Top Plate
  top_plate_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Top Plate"));
  top_plate_->SetupAttachment(RootComponent);
  top_plate_->SetRelativeLocation(FVector(8.12F, 0.0F, 22.5F));
  top_plate_->SetEnableGravity(false);
  if (top_plate_mesh.Succeeded())
  {
    top_plate_->SetStaticMesh(top_plate_mesh.Object);
  }

  // Top Frame
  top_frame_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Top Frame"));
  top_frame_->SetupAttachment(RootComponent);
  top_frame_->SetRelativeLocation(FVector(8.12F, 0.0F, 22.5F));
  top_frame_->GetBodyInstance()->SetMassOverride(5.0F);
  if (top_frame_mesh.Succeeded())
  {
    top_frame_->SetStaticMesh(top_frame_mesh.Object);
  }

  // Onboard Camera
  on_board_cam_ = CreateDefaultSubobject<UCameraComponent>(TEXT("Onbard Camera"));
  on_board_cam_->SetupAttachment(RootComponent);
  on_board_cam_->SetRelativeLocation(FVector(41.12F, 10.0F, 47.89F));
  on_board_cam_->SetRelativeScale3D(FVector(0.1F, 0.1F, 0.1F));

  // All our various data devices
  // vn100 (IMU)
  vn_100_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("vn100"));
  vn_100_->SetupAttachment(RootComponent);
  vn_100_->SetRelativeLocationAndRotation(FVector(13.2F, 0.0F, 37.42625F), FRotator(0.0F, 180.0F, 0.0F));
  vn_100_->SetEnableGravity(false);
  if (vn100_mesh.Succeeded())
  {
    vn_100_->SetStaticMesh(vn100_mesh.Object);
  }
  // NOTE: If/when we get a child class for this, attach it here.
  vn_ = CreateDefaultSubobject<UChildActorComponent>(TEXT("VN"));
  vn_->SetupAttachment(vn_100_);

  // vlp (Lidar)
  vlp_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("vlp"));
  vlp_->SetupAttachment(RootComponent);
  vlp_->SetRelativeLocationAndRotation(FVector(47.49F, 0.0F, 61.14F), FRotator(5.0F, 180.0F, 180.0F));
  vlp_->GetBodyInstance()->SetMassOverride(0.83F);
  if (vlp_mesh.Succeeded())
  {
    vlp_->SetStaticMesh(vlp_mesh.Object);
  }
  // The actual Lidar actor itself
  vlp_16_ = CreateDefaultSubobject<UChildActorComponent>(TEXT("VLP-16"));
  vlp_16_->SetupAttachment(vlp_);
  vlp_16_->SetChildActorClass(lidar_class_);
  vlp_16_->SetVisibility(false);

  // emlid (GPS)
  emlid_ = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("emlid"));
  emlid_->SetupAttachment(RootComponent);
  emlid_->SetRelativeLocationAndRotation(FVector(-18.55F, 0.0F, 55.04F), FRotator(0.0F, 45.0F, 0.0F));
  emlid_->SetEnableGravity(false);
  if (emlid_mesh.Succeeded())
  {
    emlid_->SetStaticMesh(emlid_mesh.Object);
  }

  // Load in the Engine Torque Curve now for later use
  static ConstructorHelpers::FObjectFinder<UCurveFloat>
    wheel_torque_curve_finder(TEXT("CurveFloat'/Game/Husky/Blueprints/wheelTorqueCurve.wheelTorqueCurve'"));
  if (wheel_torque_curve_finder.Succeeded())
  {
    wheel_torque_curve_ = wheel_torque_curve_finder.Object;
  }
}

FORCEINLINE void AHusky::setupWheel(UStaticMeshComponent** wheel, UPhysicsConstraintComponent** phys, FName wheel_name,
  FName phys_name, FVector pos, UStaticMesh* mesh)
{
  // Wheel itself
  *wheel = CreateDefaultSubobject<UStaticMeshComponent>(wheel_name);
  (*wheel)->SetupAttachment(RootComponent);
  (*wheel)->SetRelativeLocation(pos);
  (*wheel)->SetSimulatePhysics(true);
  (*wheel)->GetBodyInstance()->SetMassOverride(1.8F);
  (*wheel)->SetEnableGravity(false);
  (*wheel)->SetCollisionProfileName(TEXT("BlockAll"));
  // The !! thing is what the Success() method does on the FObjectFinder does. So just do it here.
  if (!!mesh)
  {
    (*wheel)->SetStaticMesh(mesh);
  }

  // Physics Constraint
  *phys = CreateDefaultSubobject<UPhysicsConstraintComponent>(phys_name);
  (*phys)->SetupAttachment(RootComponent);
  // Using this instead of pos because idk, it feels more right.
  (*phys)->SetRelativeLocation((*wheel)->RelativeLocation);
  (*phys)->SetHiddenInGame(true);
  // We know that our RootComponent is a UStaticMeshComponent, which is a UPrimitiveComponent.
  (*phys)->SetConstrainedComponents(dynamic_cast<UPrimitiveComponent*>(RootComponent), NAME_None, *wheel, NAME_None);
  (*phys)->SetDisableCollision(true);
  (*phys)->ConstraintInstance.ProfileInstance.ProjectionLinearTolerance = 0.2F;
  (*phys)->ConstraintInstance.ProfileInstance.ProjectionAngularTolerance = 0.0F;
  (*phys)->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Locked, 45.0F);
  (*phys)->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Locked, 45.0F);
}

// Called when the game starts or when spawned
void AHusky::BeginPlay()
{
  Super::BeginPlay();

  // Make sure that the external cam is the active camera
  if (!external_cam_->IsActive())
  {
    external_cam_->Activate();
  }
  if (on_board_cam_->IsActive())
  {
    on_board_cam_->Deactivate();
  }

  // Set Wheel Velocity Limits
  wheel_RL_->GetBodyInstance()->SetMaxAngularVelocityInRadians(max_velocity_ / wheel_radius_, false);
  wheel_RR_->GetBodyInstance()->SetMaxAngularVelocityInRadians(max_velocity_ / wheel_radius_, false);

  // Make sure the wheels slow down when no torque is applied
  wheel_FL_->SetAngularDamping(damping_);
  wheel_FR_->SetAngularDamping(damping_);
  wheel_RL_->SetAngularDamping(damping_);
  wheel_RR_->SetAngularDamping(damping_);

  // Save off the initial inverse transform for later use
  init_inv_trans_ = GetActorTransform().Inverse();

  // Create our timelines
  // These are events that we bind our functions that have our timeline logic in them.
  FOnTimelineEvent cmd_vel_timer, odom, clear_vars_timer;

  // The length of this timeline is the last keyframe (In our case, 0.2F)
  cmd_vel_.SetTimelineLengthMode(ETimelineLengthMode::TL_LastKeyFrame);
  cmd_vel_.SetLooping(true); // Loop!
  cmd_vel_timer.BindUFunction(this, TEXT("updateTorque")); // Bind this event to our updateTorque function
  // Place a keyframe at 0.2F, and when we hit it, fire on the cmd_vel_timer event
  cmd_vel_.AddEvent(0.2F, cmd_vel_timer);
  odom.BindUFunction(this, TEXT("handleOdomMsg")); // Bind this event to our handleOdomMsg function
  // Make 4 keyframes, 0.05F, 0.10F, 0.15F, 0.20F, and when we hit them, fire on the odom event
  for (int i = 1; i <= 4; i++)
  {
    cmd_vel_.AddEvent(0.05F * i, odom);
  }
  cmd_vel_.SetPlayRate(drive_update_freq_);
  cmd_vel_.Play();

  // We're going to have one keyframe at 0.5F, but we want the timeline length to be 1.0F
  clr_cmd_vel_.SetTimelineLength(1.0F);
  clear_vars_timer.BindUFunction(this, TEXT("clearTwstValues"));
  clr_cmd_vel_.AddEvent(0.5F, clear_vars_timer);

  // ----- START ROS INTEGRATION CODE -----
  // Setup timeline for IMU & GPS Odom msgs
  FOnTimelineEvent imu_event, gps_event;

  imu_pub_line_.SetTimelineLengthMode(ETimelineLengthMode::TL_LastKeyFrame);
  imu_pub_line_.SetLooping(true);
  imu_event.BindUFunction(this, TEXT("pubImuMsg"));
  imu_pub_line_.AddEvent(1.0F, imu_event);
  imu_pub_line_.SetPlayRate(imu_pub_rate_);
  imu_pub_line_.Play();

  gps_pub_line_.SetTimelineLengthMode(ETimelineLengthMode::TL_LastKeyFrame);
  gps_pub_line_.SetLooping(true);
  gps_event.BindUFunction(this, TEXT("pubGpsOdomMsg"));
  gps_pub_line_.AddEvent(1.0F, gps_event);
  gps_pub_line_.SetPlayRate(gps_pub_rate_);
  gps_pub_line_.Play();

  // Make sure our system clock keeps clicking along
  FROSTime::SetUseSimTime(false);
  FWorldDelegates::OnWorldTickStart.AddUObject(this, &AHusky::pubClock);

  // Get the ROS game instance
  UROSIntegrationGameInstance* rosinst = Cast<UROSIntegrationGameInstance>(GetGameInstance());
  // Setup the topic
  clock_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  twist_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  pt_cld2_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  imu_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  tf2_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  gps_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());
  wheel_odom_topic_ = NewObject<UTopic>(this, UTopic::StaticClass());

  // Initialize all of the Topics
  clock_topic_->Init(rosinst->ROSIntegrationCore, TEXT("/clock"), TEXT("rosgraph_msgs/Clock"), 3);
  twist_topic_->Init(rosinst->ROSIntegrationCore, twist_sub_topic_name_, TEXT("geometry_msgs/Twist"), 5);
  pt_cld2_topic_->Init(rosinst->ROSIntegrationCore, pt_cld2_pub_name_, TEXT("sensor_msgs/PointCloud2"), 1);
  imu_topic_->Init(rosinst->ROSIntegrationCore, imu_pub_topic_name_, TEXT("sensor_msgs/Imu"), 1);
  tf2_topic_->Init(rosinst->ROSIntegrationCore, tf2_pub_topic_name_, TEXT("tf2_msgs/TFMessage"), 1);
  gps_topic_->Init(rosinst->ROSIntegrationCore, gps_pub_name_, TEXT("nav_msgs/Odometry"), 1);
  wheel_odom_topic_->Init(rosinst->ROSIntegrationCore, wheel_pub_name_, TEXT("nav_msgs/Odometry"), 1);

  // Advertise our publishers
  clock_topic_->Advertise();
  pt_cld2_topic_->Advertise();
  imu_topic_->Advertise();
  tf2_topic_->Advertise();
  gps_topic_->Advertise();
  wheel_odom_topic_->Advertise();

  // Twist Msg Subscriber
  // Create a std::function callback object
  std::function<void(TSharedPtr<FROSBaseMsg>)> TwistCallback = [this](TSharedPtr<FROSBaseMsg> msg) -> void
  {
    auto Concrete = StaticCastSharedPtr<ROSMessages::geometry_msgs::Twist>(msg);
    if (Concrete.IsValid())
    {
      this->linear_ = FVector(Concrete->linear.x, Concrete->linear.y, Concrete->linear.z);
      this->angular_ = FVector(Concrete->angular.x, Concrete->angular.y, Concrete->angular.z);
      this->is_accelerating_ = true;
      this->clr_cmd_vel_.PlayFromStart(); // Start the timeline to clear these values after a little bit
    }
  };
  // Subscribe to the topic
  twist_topic_->Subscribe(TwistCallback);
}

#if WITH_EDITOR
// Make sure things that get set in constructors/BeginPlay get updated if they're updated in the Editor.
// NOTE: I don't think we need to worry about BeginPlay, but I'm not 100% sure, so I have those vars here for now.
void AHusky::PostEditChangeProperty(struct FPropertyChangedEvent &Event)
{
  FName prop_name = Event.GetPropertyName();
  if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, max_velocity_) ||
    prop_name == GET_MEMBER_NAME_CHECKED(AHusky, wheel_radius_))
  {
    wheel_RL_->SetPhysicsMaxAngularVelocityInRadians(max_velocity_ / wheel_radius_);
    wheel_RR_->SetPhysicsMaxAngularVelocityInRadians(max_velocity_ / wheel_radius_);
  }
  else if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, damping_))
  {
    wheel_FL_->SetAngularDamping(damping_);
    wheel_FR_->SetAngularDamping(damping_);
    wheel_RL_->SetAngularDamping(damping_);
    wheel_RR_->SetAngularDamping(damping_);
  }
  else if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, drive_update_freq_))
  {
    cmd_vel_.SetPlayRate(drive_update_freq_);
  }
  else if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, lidar_class_))
  {
    vlp_16_->SetChildActorClass(lidar_class_);
    vlp_16_->DestroyChildActor();
    vlp_16_->CreateChildActor();
  }
  else if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, imu_pub_rate_))
  {
    imu_pub_line_.SetPlayRate(imu_pub_rate_);
  }
  else if (prop_name == GET_MEMBER_NAME_CHECKED(AHusky, gps_pub_rate_))
  {
    gps_pub_line_.SetPlayRate(gps_pub_rate_);
  }

  Super::PostEditChangeProperty(Event);
}
#endif // WITH_EDITOR

// Called every frame
void AHusky::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

  // Set the new engine torque
  if (wheel_torque_curve_ != nullptr)
  {
    engine_torque_ = wheel_torque_curve_->GetFloatValue(rpm_) * current_gain_;
  }
  
  // RPM Control
  if (is_accelerating_)
  {
    rpm_ = FMath::Min(rpm_limit_, rpm_ + accel_factor_ * DeltaTime);
  }
  else
  {
    rpm_ = FMath::Max(0.0F, rpm_ - decel_factor_ * DeltaTime);
  }

  // Feed the new dt to the timelines.
  cmd_vel_.TickTimeline(DeltaTime);
  clr_cmd_vel_.TickTimeline(DeltaTime);
  imu_pub_line_.TickTimeline(DeltaTime);
  gps_pub_line_.TickTimeline(DeltaTime);
}

// ----- BEGIN ROS INTEGRATION STUFF -----
// Tick to make sure our clock topic is kept published and up to date
void AHusky::pubClock(ELevelTick tick_type, float dt)
{
  // send /clock topic to let everyone know what time it is...
  TSharedPtr<ROSMessages::rosgraph_msgs::Clock> clk_msg(new ROSMessages::rosgraph_msgs::Clock(FROSTime::Now()));
  clock_topic_->Publish(clk_msg);
}

// pointCloud2 message publisher function
void AHusky::pointCloud2Msg(const float &data, FString frame_id, int32 cloud_size, int32 cloud_seq)
{
  // Create an instance of our point cloud message
  TSharedPtr<ROSMessages::sensor_msgs::PointCloud2> pt_cld2_msg(new ROSMessages::sensor_msgs::PointCloud2());

  //-------------------Message header creation and population-----------//

    // Create our data fields array
  namespace pt_cld2 = ROSMessages::sensor_msgs;
  pt_cld2::PointCloud2::PointField pointDataField[3] = {{"x",0,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            {"y",4,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            {"z",8,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1}};//,
                            //{"intensity",16,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            //{"ring",20,pt_cld2::PointCloud2::PointField::EType::UINT16,1}};

  // Data fields
  pt_cld2_msg->fields = TArray<pt_cld2::PointCloud2::PointField>(pointDataField, 3);

  // Because the size of the array passed in is x followed by y followed by z. (i.e. data is concatenated)
  cloud_size = cloud_size / 3;

  cloud_size = std::max((size_t)1, (size_t)cloud_size);

  // Is the data bigendian?
  pt_cld2_msg->is_bigendian = false;
  // Char step size
  pt_cld2_msg->point_step = 12;
  pt_cld2_msg->row_step = cloud_size * (pt_cld2_msg->point_step);

  // Set the height and width of the message, based on structured/unstructured
  pt_cld2_msg->height = 1;
  pt_cld2_msg->width = cloud_size;

  // True if there are no invalid points
  pt_cld2_msg->is_dense = false;

  // Header(uint32 seq, FROSTime time, FString frame_id)
  pt_cld2_msg->header = ROSMessages::std_msgs::Header(cloud_seq, FROSTime::Now(), frame_id);

  // Now apply data
  float *floatData = const_cast<float*>(&data);
  pt_cld2_msg->data_ptr = reinterpret_cast<const uint8*>(floatData);

  // Publish.....
  pt_cld2_topic_->Publish(pt_cld2_msg);
}

// TF2 message Publisher
void AHusky::tf2Msg(FTransform pt_cld_transform, FString frame_id, int32 msg_seq)
{
  // Create an instance of our TF2 message
  TSharedPtr<ROSMessages::tf2_msgs::TFMessage> tf2_message(new ROSMessages::tf2_msgs::TFMessage());
  TSharedPtr<ROSMessages::geometry_msgs::TransformStamped>
    tf2StampMessage(new ROSMessages::geometry_msgs::TransformStamped());

  //-------------------Message header creation and population-----------//

  // Header(uint32 seq, FROSTime time, FString frame_id)
  tf2StampMessage->header = ROSMessages::std_msgs::Header(msg_seq, FROSTime::Now(), frame_id);
  static FString child_frame_id("ref_pt_cld_transform"); // Hooray, saving memory!
  tf2StampMessage->child_frame_id = child_frame_id;
  // Flip to right hand from UE4's left handed space
  FVector tf = pt_cld_transform.GetTranslation() / 100.0F;
  tf2StampMessage->transform.translation = ROSMessages::geometry_msgs::Vector3(tf.X, -tf.Y, tf.Z);
  tf2StampMessage->transform.rotation = ROSMessages::geometry_msgs::Quaternion(pt_cld_transform.GetRotation());

  // Add the transform to the list
  tf2_message->transforms.Add(*tf2StampMessage);

  // Publish.....
  tf2_topic_->Publish(tf2_message);
}

// ----- END ROS INTEGRATION STUFF -----

void AHusky::updateTorque()
{
  FVector tmp = wheel_RR_->GetRightVector();
  wheel_FR_->AddTorqueInRadians(tmp * engine_torque_ * (linear_.X + angular_.Z), NAME_None, true);
  wheel_RR_->AddTorqueInRadians(tmp * engine_torque_ * (linear_.X + angular_.Z), NAME_None, true);
  tmp = wheel_RL_->GetRightVector();
  wheel_FL_->AddTorqueInRadians(tmp * engine_torque_ * (linear_.X - angular_.Z), NAME_None, true);
  wheel_RL_->AddTorqueInRadians(tmp * engine_torque_ * (linear_.X - angular_.Z), NAME_None, true);
}

void AHusky::handleOdomMsg()
{
  // Shorten some very long fully-qualified class names. Because I really don't want to break up lines.
  using namespace ROSMessages;
  // Static to save memory
  static FString odom_frame(TEXT("odom"));

  // Create an instance of our wheel Odom message
  TSharedPtr<nav_msgs::Odometry> wheel_odom_message(new nav_msgs::Odometry());
  
  //-------------------Message header creation and population-----------//
  
  // Header(uint32 seq, FROSTime time, FString frame_id)
  wheel_odom_message->header = std_msgs::Header(0, FROSTime::Now(), odom_frame);
  
  // No child frame
  //wheel_odom_message->child_frame_id = '';
  
  // Create a pose instance
  TSharedPtr<geometry_msgs::PoseWithCovariance>
    pose_w_cov(new geometry_msgs::PoseWithCovariance());
  TSharedPtr<geometry_msgs::Pose> pose(new geometry_msgs::Pose());
  TSharedPtr<geometry_msgs::TwistWithCovariance>
    twist_w_cov(new geometry_msgs::TwistWithCovariance());
  
  // Position for Perfect Odom
  FVector perfect_pos = init_inv_trans_.TransformPosition(GetActorLocation()) / 100.0F;
  perfect_pos.Y *= -1;
  // Position for Wheel Odom
  FVector position = perfect_pos + (FMath::VRand() * pos_std_dev_);
  // Populating our Pose portion of Odom
  pose->position = geometry_msgs::Point(position);
  
  // Orientation for Perfect Odom
  FQuat perfect_orientation = init_inv_trans_.TransformRotation(GetActorRotation().Quaternion());
  // Orientation for Wheel Odom
  FVector tmp = FMath::VRand() * rot_std_dev_;
  FQuat orientation = perfect_orientation * FRotator(tmp.Y, tmp.Z, tmp.X).Quaternion();
  // Orientation needs converted to ENU
  float x = orientation.Y;
  float y = orientation.X;
  float z = -orientation.Z;
  float w = orientation.W;
  
  pose->orientation = geometry_msgs::Quaternion(x,y,z,w);
  pose_w_cov->pose = *pose;
  
  // Populating our Twist portion of Odom
  UPrimitiveComponent* root_primitive = dynamic_cast<UPrimitiveComponent*>(RootComponent);
  if (root_primitive != nullptr)
  {
    twist_w_cov->twist.linear = geometry_msgs::Vector3(root_primitive->GetPhysicsLinearVelocity() / 100.0F);
  }
  twist_w_cov->twist.angular = geometry_msgs::Vector3(0.0, 0.0, angular_V_.Z);
  
  // Set up the covariances	
  double covariance[36] = {0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03};
  pose_w_cov->covariance = TArray<double>(covariance, 36);
  twist_w_cov->covariance = TArray<double>(covariance, 36);
  
  // Populate the Odom message
  wheel_odom_message->pose = *pose_w_cov;
  wheel_odom_message->twist = *twist_w_cov;
  
  // Output our linear and angular velocity
  // Compute the new dt
  float cur_time = GetWorld()->GetRealTimeSeconds();
  float dt = cur_time - old_time_;
  old_time_ = cur_time;
  if(dt > 0)
  {
    // Vertical pipe is the Dot Product.
    float r = wheel_FR_->GetPhysicsAngularVelocityInRadians() | wheel_FR_->GetRightVector();
    float l = wheel_FL_->GetPhysicsAngularVelocityInRadians() | wheel_FL_->GetRightVector();
    float direction = FMath::Sign<float>(wheel_radius_ * (r + l) / 2.0F);
    linear_V_ = FVector(direction * FVector::Dist2D(position, old_position_) / dt, 0, 0);
    angular_V_ = (orientation.Vector() - old_orientation_.Vector()) / dt;
    // Flip angular_vel to ENU convection
    float x = angular_V_.Y;
    float y = angular_V_.X;
    float z = -angular_V_.Z;
    angular_V_ = FVector(x,y,z);
  }
  old_position_ = position;
  old_orientation_ = orientation;

  // Publish.....	
  wheel_odom_topic_->Publish(wheel_odom_message);
}

// Called from a timeline
void AHusky::clearTwstValues()
{
  is_accelerating_ = false;
  linear_ = FVector::ZeroVector;
  angular_ = FVector::ZeroVector;
}

// ROS Timeline methods
void AHusky::pubImuMsg()
{
  // Shorten some very long fully-qualified class names. Because I really don't want to break up lines.
  using namespace ROSMessages;
  // Static to save memory
  static FString imu2_frame(TEXT("imu2"));
  if (GetWorld()->GetTimeSeconds() > 1.0F)
  {
    // Create an instance of our Imu message
    TSharedPtr<sensor_msgs::Imu> imu_msg(new sensor_msgs::Imu());

    //-------------------Message header creation and population-----------//

    // Header(uint32 seq, FROSTime time, FString frame_id)
    imu_msg->header = std_msgs::Header(0, FROSTime::Now(), imu2_frame);

    // Orientation needs converted to ENU
    FQuat orient = vn_100_->GetComponentQuat();
    float x = orient.Y;
    float y = orient.X;
    float z = -orient.Z;
    float w = orient.W;

    // Compute dt
    float cur_time = GetWorld()->GetRealTimeSeconds();
    float dt = cur_time - old_time_2_;
    old_time_2_ = cur_time;

    // Populate the message
    imu_msg->orientation = geometry_msgs::Quaternion(x, y, z, w);
    imu_msg->angular_velocity = geometry_msgs::Vector3(vn_100_->GetPhysicsAngularVelocityInRadians());
    imu_msg->linear_acceleration = geometry_msgs::Vector3(vn_100_->GetPhysicsLinearVelocity() / dt / 100.0F);

    //UE_LOG(LogROS, Warning, TEXT("R: %f P: %f Y: %f"),orient.Roll, orient.Pitch, orient.Yaw);

    // Set up the covariances	
    double covariance[9] = { 0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03 };
    imu_msg->orientation_covariance = TArray<double>(covariance, 9);
    imu_msg->angular_velocity_covariance = TArray<double>(covariance, 9);
    imu_msg->linear_acceleration_covariance = TArray<double>(covariance, 9);

    // Publish.....
    imu_topic_->Publish(imu_msg);
  }
}

void AHusky::pubGpsOdomMsg()
{
  // Static to save memory
  static FString odom_frame(TEXT("odom"));
  // Create an instance of our gps Odom message
  TSharedPtr<ROSMessages::nav_msgs::Odometry> gps_odom_message(new ROSMessages::nav_msgs::Odometry());

  //-------------------Message header creation and population-----------//

  // Header(uint32 seq, FROSTime time, FString frame_id)
  gps_odom_message->header = ROSMessages::std_msgs::Header(0, FROSTime::Now(), odom_frame);

  // No child frame
  //gps_odom_message->child_frame_id = '';

  // Create a pose instance
  TSharedPtr<ROSMessages::geometry_msgs::PoseWithCovariance>
    pose_w_cov(new ROSMessages::geometry_msgs::PoseWithCovariance());
  TSharedPtr<ROSMessages::geometry_msgs::Pose> pose(new ROSMessages::geometry_msgs::Pose());
  TSharedPtr<ROSMessages::geometry_msgs::TwistWithCovariance>
    twist_w_cov(new ROSMessages::geometry_msgs::TwistWithCovariance());
  // Fill out our position
  FVector position = init_inv_trans_.TransformPosition(GetActorLocation()) / 100.0F;
  position.Y *= -1;
  pose->position = ROSMessages::geometry_msgs::Point(position);
  pose_w_cov->pose = *pose;

  // Set up the covariances	
  double covariance[36] = {0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01};
  pose_w_cov->covariance = TArray<double>(covariance, 36);
  twist_w_cov->covariance = TArray<double>(covariance, 36);

  // Populate the Odom message
  gps_odom_message->pose = *pose_w_cov;
  gps_odom_message->twist = *twist_w_cov;

  // Publish.....
  gps_topic_->Publish(gps_odom_message);
}

// Called to bind functionality to input
void AHusky::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
  Super::SetupPlayerInputComponent(PlayerInputComponent);

  PlayerInputComponent->BindKey(EKeys::MouseScrollDown, EInputEvent::IE_Pressed,  this, &AHusky::addLengthToSA);
  PlayerInputComponent->BindKey(EKeys::MouseScrollUp,   EInputEvent::IE_Pressed,  this, &AHusky::subLengthFromSA);
  PlayerInputComponent->BindKey(EKeys::Tab,             EInputEvent::IE_Released, this, &AHusky::toggleCamera);
}

// Callback functions for the above functions for keybindings
void AHusky::addLengthToSA()
{
  spring_arm_->TargetArmLength += 30.0F;
}

void AHusky::subLengthFromSA()
{
  spring_arm_->TargetArmLength -= 30.0F;
  // Make sure to clamp it at 0.
  // The engine might do this for us, but I just want to make 100% sure.
  if (spring_arm_->TargetArmLength < 0.0F)
  {
    spring_arm_->TargetArmLength = 0.0F;
  }
}

void AHusky::toggleCamera()
{
  external_cam_->SetActive(!external_cam_->IsActive());
  on_board_cam_->SetActive(!on_board_cam_->IsActive());
}
