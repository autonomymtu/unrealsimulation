/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

// ROS Includes
#include "ros_actor.h"

// ROSIntegration plugin includes
#include "Clock.h"
#include "Engine/World.h"
#include "ROSIntegration/Classes/RI/Topic.h"
#include "ROSIntegration/Classes/ROSIntegrationGameInstance.h"
#include "ROSIntegration/Public/std_msgs/String.h"
#include "ROSIntegration/Public/rosgraph_msgs/Clock.h"
#include "ROSIntegration/Public/sensor_msgs/PointCloud2.h"
#include "ROSIntegration/Public/geometry_msgs/Twist.h"
#include "ROSIntegration/Public/geometry_msgs/Vector3.h"
#include "ROSIntegration/Public/nav_msgs/Odometry.h"
#include "ROSIntegration/Public/sensor_msgs/Imu.h"
#include "ROSIntegration/Public/tf2_msgs/TFMessage.h"

static FROSTime originalTime;

// Sets default values
ADEPRECATED_RosActor::ADEPRECATED_RosActor() :
imu_pub_topic_name_(TEXT("/imu/data")),
pt_cld2_pub_name_(TEXT("/velodyne_points")),
gps_pub_name_(TEXT("/odometry/gps")),
wheel_pub_name_(TEXT("/husky_velocity_controller/odom")),
twist_sub_topic_name_(TEXT("/cmd_vel")),
tf2_pub_topic_name_(TEXT("/tf")),
old_position_(ForceInit),
old_orientation_(ForceInit)
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADEPRECATED_RosActor::BeginPlay()
{
  Super::BeginPlay();
  FROSTime originalTime = FROSTime::Now();
  
  // Make sure our system clock keeps clicking along
  FROSTime::SetUseSimTime(false);
  FWorldDelegates::OnWorldTickStart.AddUObject(this, &ADEPRECATED_RosActor::OnWorldTickStart);
  
  // Get the ROS game instance
  UROSIntegrationGameInstance* rosinst = Cast<UROSIntegrationGameInstance>(GetGameInstance());
  // Setup the topic
  clock_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  pt_cld2_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  imu_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  tf2_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  gps_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  wheel_odom_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  
  // Initialize all of the Topics
  clock_topic_->Init(rosinst->ROSIntegrationCore,TEXT("/clock"), TEXT("rosgraph_msgs/Clock"), 3);
  pt_cld2_topic_->Init(rosinst->ROSIntegrationCore, pt_cld2_pub_name_, TEXT("sensor_msgs/PointCloud2"), 1);
  imu_topic_->Init(rosinst->ROSIntegrationCore, imu_pub_topic_name_, TEXT("sensor_msgs/Imu"), 1);
  tf2_topic_->Init(rosinst->ROSIntegrationCore, tf2_pub_topic_name_, TEXT("tf2_msgs/TFMessage"), 1);
  gps_topic_->Init(rosinst->ROSIntegrationCore, gps_pub_name_, TEXT("nav_msgs/Odometry"), 1);
  wheel_odom_topic_->Init(rosinst->ROSIntegrationCore, wheel_pub_name_, TEXT("nav_msgs/Odometry"), 1);

  // Advertise our publishers
  clock_topic_->Advertise();
  pt_cld2_topic_->Advertise();
  imu_topic_->Advertise();
  tf2_topic_->Advertise();
  gps_topic_->Advertise();
  wheel_odom_topic_->Advertise();
}

// Called every frame
void ADEPRECATED_RosActor::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
  // Publish ROS clock
  //rosTimeOutput();
}

// Tick to make sure our clock topic is kept published and up to date
void ADEPRECATED_RosActor::OnWorldTickStart(ELevelTick TickType, float DeltaTime)
{
  FROSTime now = FROSTime::Now();

  // send /clock topic to let everyone know what time it is...
  TSharedPtr<ROSMessages::rosgraph_msgs::Clock> clk_msg(new ROSMessages::rosgraph_msgs::Clock(now));
  clock_topic_->Publish(clk_msg);
}

    
// Activate our String Subscriber Function
void ADEPRECATED_RosActor::twistMsgSub()
{
  UROSIntegrationGameInstance* rosinst = Cast<UROSIntegrationGameInstance>(GetGameInstance());
  twist_topic_ = NewObject<UTopic>(UTopic::StaticClass());
  twist_topic_->Init(rosinst->ROSIntegrationCore, twist_sub_topic_name_, TEXT("geometry_msgs/Twist"),5);
  
  // Create a std::function callback object
  std::function<void(TSharedPtr<FROSBaseMsg>)> TwistCallback = [&](TSharedPtr<FROSBaseMsg> msg) -> void
  {
    auto Concrete = StaticCastSharedPtr<ROSMessages::geometry_msgs::Twist>(msg);
    if (Concrete.IsValid())
    {
      //Unload the data
      FVector linear = FVector((*Concrete).linear.x,(*Concrete).linear.y,(*Concrete).linear.z);
      FVector angular = FVector((*Concrete).angular.x,(*Concrete).angular.y,(*Concrete).angular.z);
      
      if (twist_msg_dispatcher_.IsBound())
      {
        twist_msg_dispatcher_.Broadcast(linear, angular);
      }
    }
  };
  
  // Subscribe to the topic
  twist_topic_->Subscribe(TwistCallback);
}

// pointCloud2 message publisher function
void ADEPRECATED_RosActor::pointCloud2Msg(const float &data, FString frame_id, int32 cloud_size, int32 cloud_seq)
{
  // Create an instance of our point cloud message
  TSharedPtr<ROSMessages::sensor_msgs::PointCloud2> pt_cld2_msg(new ROSMessages::sensor_msgs::PointCloud2());
  
  //-------------------Message header creation and population-----------//
  
    // Create our data fields array
  namespace pt_cld2 = ROSMessages::sensor_msgs;
  pt_cld2::PointCloud2::PointField pointDataField[3] = {{"x",0,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            {"y",4,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            {"z",8,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1}};//,
                            //{"intensity",16,pt_cld2::PointCloud2::PointField::EType::FLOAT32,1},
                            //{"ring",20,pt_cld2::PointCloud2::PointField::EType::UINT16,1}};
  
  // Data fields
  pt_cld2_msg->fields = TArray<pt_cld2::PointCloud2::PointField>(pointDataField, 3);
  
  // Because the size of the array passed in is x followed by y followed by z. (i.e. data is concatenated)
  cloud_size = cloud_size/3;
  
  cloud_size = std::max((size_t)1,(size_t)cloud_size);
  
  // Is the data bigendian?
  pt_cld2_msg->is_bigendian = false;
  // Char step size
  pt_cld2_msg->point_step = 12;
  pt_cld2_msg->row_step = cloud_size*(pt_cld2_msg->point_step);
  
  // Set the height and width of the message, based on structured/unstructured
  pt_cld2_msg->height = 1;
  pt_cld2_msg->width = cloud_size;
  
  // True if there are no invalid points
  pt_cld2_msg->is_dense = false;
  
  // Header(uint32 seq, FROSTime time, FString frame_id)
  pt_cld2_msg->header = ROSMessages::std_msgs::Header(cloud_seq, FROSTime::Now(), frame_id);
  
  // Now apply data
  float *floatData = const_cast<float*>(&data);
  pt_cld2_msg->data_ptr = reinterpret_cast<const uint8*>(floatData);

  // Publish.....
  pt_cld2_topic_->Publish(pt_cld2_msg);
}


// TF2 message Publisher
void ADEPRECATED_RosActor::tf2Msg(FTransform pt_cld_transform, FString frame_id, int32 msg_seq)
{
  // Create an instance of our TF2 message
  TSharedPtr<ROSMessages::tf2_msgs::TFMessage> tf2_message(new ROSMessages::tf2_msgs::TFMessage());
  TSharedPtr<ROSMessages::geometry_msgs::TransformStamped>
    tf2StampMessage(new ROSMessages::geometry_msgs::TransformStamped());
  
  //-------------------Message header creation and population-----------//
  
  // Header(uint32 seq, FROSTime time, FString frame_id)
  tf2StampMessage->header = ROSMessages::std_msgs::Header(msg_seq, FROSTime::Now(), frame_id);
  tf2StampMessage->child_frame_id = FString("ref_pt_cld_transform");
  // Flip to right hand from UE4's left handed space
  static FVector forward_normal = FVector::ForwardVector.GetSafeNormal();
  FVector tf = pt_cld_transform.GetTranslation().RotateAngleAxis(180.0f, forward_normal)/100;
  tf2StampMessage->transform.translation = ROSMessages::geometry_msgs::Vector3(tf.X,tf.Y,-tf.Z);
  tf2StampMessage->transform.rotation = ROSMessages::geometry_msgs::Quaternion(pt_cld_transform.GetRotation());
  
  // Add the transform to the list
    tf2_message->transforms.Add(*tf2StampMessage);
  
  // Publish.....
  tf2_topic_->Publish(tf2_message);
}

// GPS odometry message function
void ADEPRECATED_RosActor::gpsOdomMsg(const FVector &position, FString frame_id, int32 msg_seq)
{
  // Create an instance of our gps Odom message
  TSharedPtr<ROSMessages::nav_msgs::Odometry> gps_odom_message(new ROSMessages::nav_msgs::Odometry());
  
  //-------------------Message header creation and population-----------//
  
  // Header(uint32 seq, FROSTime time, FString frame_id)
  gps_odom_message->header = ROSMessages::std_msgs::Header(msg_seq, FROSTime::Now(), frame_id);
  
  // No child frame
  //gps_odom_message->child_frame_id = '';
  
  // Create a pose instance
  TSharedPtr<ROSMessages::geometry_msgs::PoseWithCovariance>
    pose_w_cov(new ROSMessages::geometry_msgs::PoseWithCovariance());
  TSharedPtr<ROSMessages::geometry_msgs::Pose> pose(new ROSMessages::geometry_msgs::Pose());
  TSharedPtr<ROSMessages::geometry_msgs::TwistWithCovariance>
    twist_w_cov(new ROSMessages::geometry_msgs::TwistWithCovariance());
  // Fill out our position
  pose->position = ROSMessages::geometry_msgs::Point(position);
  pose_w_cov->pose = *pose;
  
  // Set up the covariances	
  double covariance[36] = {0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01};
  pose_w_cov->covariance = TArray<double>(covariance, 36);
  twist_w_cov->covariance = TArray<double>(covariance, 36);
  
  // Populate the Odom message
  gps_odom_message->pose = *pose_w_cov;
  gps_odom_message->twist = *twist_w_cov;

  // Publish.....
  gps_topic_->Publish(gps_odom_message);
}

// Wheel odometry message function, not entirely done as velocities etc. need work
void ADEPRECATED_RosActor::wheelOdomMsg(const float direction, const FVector &position, const FRotator &orientation,
  const FVector &linear_twist, const FVector &angular_twist, const float &dt, FVector &angular_vel,
  FVector &linear_vel, FString frame_id, int32 msg_seq) 
{
  // Create an instance of our wheel Odom message
  TSharedPtr<ROSMessages::nav_msgs::Odometry> wheel_odom_message(new ROSMessages::nav_msgs::Odometry());
  
  //-------------------Message header creation and population-----------//
  
  // Header(uint32 seq, FROSTime time, FString frame_id)
  wheel_odom_message->header = ROSMessages::std_msgs::Header(msg_seq, FROSTime::Now(), frame_id);
  
  // No child frame
  //wheel_odom_message->child_frame_id = '';
  
  // Create a pose instance
  TSharedPtr<ROSMessages::geometry_msgs::PoseWithCovariance>
    pose_w_cov(new ROSMessages::geometry_msgs::PoseWithCovariance());
  TSharedPtr<ROSMessages::geometry_msgs::Pose> pose(new ROSMessages::geometry_msgs::Pose());
  TSharedPtr<ROSMessages::geometry_msgs::TwistWithCovariance>
    twist_w_cov(new ROSMessages::geometry_msgs::TwistWithCovariance());
  
  // Populating our Pose portion of Odom
  pose->position = ROSMessages::geometry_msgs::Point(position);
  
  // Orientation needs converted to ENU
  float x = orientation.Quaternion().Y;
  float y = orientation.Quaternion().X;
  float z = -orientation.Quaternion().Z;
  float w = orientation.Quaternion().W;
  
  pose->orientation = ROSMessages::geometry_msgs::Quaternion(x,y,z,w);
  pose_w_cov->pose = *pose;
  
  // Populating our Twist portion of Odom
  twist_w_cov->twist.linear = ROSMessages::geometry_msgs::Vector3(linear_twist);
  twist_w_cov->twist.angular = ROSMessages::geometry_msgs::Vector3(angular_twist);
  
  // Set up the covariances	
  double covariance[36] = {0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0,
    0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.03};
  pose_w_cov->covariance = TArray<double>(covariance, 36);
  twist_w_cov->covariance = TArray<double>(covariance, 36);
  
  // Populate the Odom message
  wheel_odom_message->pose = *pose_w_cov;
  wheel_odom_message->twist = *twist_w_cov;
  
  // Output our linear and angular velocity
  if(dt > 0)
  {
    linear_vel = FVector(direction*FVector::Dist2D(position,old_position_)/dt,0,0);
    angular_vel = (orientation.Vector() - old_orientation_.Vector())/dt;
    // Flip angular_vel to ENU convection
    float x = angular_vel.Y;
    float y = angular_vel.X;
    float z = -angular_vel.Z;
    angular_vel = FVector(x,y,z);
  }
  old_position_ = position;
  old_orientation_ = orientation;

  // Publish.....	
  wheel_odom_topic_->Publish(wheel_odom_message);
}

// IMU message function
void ADEPRECATED_RosActor::imuMsg(const FVector &accel, const FVector &angular, const FRotator &orient, FString frame_id,
  int32 msg_seq) 
{
  // Create an instance of our Imu message
  TSharedPtr<ROSMessages::sensor_msgs::Imu> imu_msg(new ROSMessages::sensor_msgs::Imu());
  
  //-------------------Message header creation and population-----------//

  // Header(uint32 seq, FROSTime time, FString frame_id)
  imu_msg->header = ROSMessages::std_msgs::Header(msg_seq, FROSTime::Now(), frame_id);
  
  // Orientation needs converted to ENU
  float x = orient.Quaternion().Y;
  float y = orient.Quaternion().X;
  float z = -orient.Quaternion().Z;
  float w = orient.Quaternion().W;
    
  // Populate the message
  imu_msg->orientation = ROSMessages::geometry_msgs::Quaternion(x,y,z,w);
  imu_msg->angular_velocity = ROSMessages::geometry_msgs::Vector3(angular);
  imu_msg->linear_acceleration = ROSMessages::geometry_msgs::Vector3(accel);
  
  //UE_LOG(LogROS, Warning, TEXT("R: %f P: %f Y: %f"),orient.Roll, orient.Pitch, orient.Yaw);
  
  // Set up the covariances	
  double covariance[9] = {0.03, 0.0, 0.0, 0.0, 0.03, 0.0, 0.0, 0.0, 0.03};
  imu_msg->orientation_covariance = TArray<double>(covariance, 9);
  imu_msg->angular_velocity_covariance = TArray<double>(covariance, 9);
  imu_msg->linear_acceleration_covariance = TArray<double>(covariance, 9);

  // Publish.....
  imu_topic_->Publish(imu_msg);
}