/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

#include "Husky_KRC.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Husky_KRC, "Husky_KRC" );
