/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

using UnrealBuildTool;

public class Husky_KRC : ModuleRules
{
  public Husky_KRC(ReadOnlyTargetRules Target) : base(Target)
  {
    PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

    PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Slate",
        "ROSIntegration" });

    PublicIncludePaths.AddRange(new string[] { "ROSIntegration/Public", "ROSIntegration/Classes" });

    PrivateDependencyModuleNames.AddRange(new string[] {  });
  }
}
