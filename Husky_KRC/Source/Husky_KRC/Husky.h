/*
* Copyright (c) Michigan Technological University (MTU)
* All rights reserved.
* Part of the Automotive Research Center (ARC) project by MTU.
* This project is distributed under the MIT License.
* A copy should have been included with the source distribution.
* If not, you can obtain a copy at https://opensource.org/licenses/MIT
*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "Components/ChildActorComponent.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TimelineComponent.h"
#include "Curves/CurveFloat.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/SpringArmComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "ROSIntegration/Classes/RI/Topic.h"
#include "Templates/SubclassOf.h"
#include "Lidar.h"
#include "ROS_actor.h"
#include "Husky.generated.h"

UCLASS()
class HUSKY_KRC_API AHusky : public APawn
{
  GENERATED_BODY()
  
public:
  // Sets default values for this pawn's properties
  AHusky();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

#if WITH_EDITOR
  // Called when a UPROPERTY is changed in the editor
  virtual void PostEditChangeProperty(struct FPropertyChangedEvent &Event) override;
#endif // WITH_EDITOR

public:	
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Called to bind functionality to input
  virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  // ROS Stuff
  // pointCloud2 publisher. Data port expects vector in x,y,z order concatenated.
  UFUNCTION(BlueprintCallable, Category="Husky|ROS", meta=(DisplayName="Publish PointCloud2 Msg"))
  void pointCloud2Msg(const float &data, FString frame_id, int32 cloud_size, int32 cloud_seq);

  // Transform publisher, current publishing reference point cloud transforms
  UFUNCTION(BlueprintCallable, Category="Husky|ROS", meta=(DisplayName="Publish TF2 Msg"))
  void tf2Msg(FTransform pt_cld_transform, FString frame_id, int32 msg_seq);

  // Sets the maximum velocity for the Husky.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Husky Max Velocity",
    ClampMin="0.0"))
  float max_velocity_;

  // Sets the radius of all the wheels on the Husky.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Wheel Radius", ClampMin="0.0"))
  float wheel_radius_;

  // Sets the angular damping for all the wheels on the Husky.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Damping", ClampMin="0.0"))
  float damping_;

  // Sets the frequency for how often torque is applied to the wheels and how often Odom messages are sent over ROS.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Drive Update Frequency",
    ClampMin="0.0"))
  float drive_update_freq_;

  // Sets the multiplier used when calculating Wheel Torque.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Current Gain", ClampMin="0.0"))
  float current_gain_;

  // Sets the maximum RPM for the Husky.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="RPM Limit", ClampMin="0.0"))
  float rpm_limit_;

  // Sets how fast the Husky accelerates.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Acceleration Factor",
    ClampMin="0.0"))
  float accel_factor_;

  // Sets how fast the Husky decelerates.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Deceleration Factor",
    ClamMin="0.0"))
  float decel_factor_;

  // Sets the noise for position calculation for the Wheel Odom messages.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Position Standard Deviation",
    ClampMin="0.0"))
  float pos_std_dev_;

  // Sets the noise for orientation calculation for the Wheel Odom messages.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Rotation Standard Deviation",
    ClampMin="0.0"))
  float rot_std_dev_;

  // Which C++/Blueprint class to use for the Lidar sub-actor.
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky", meta=(DisplayName="Lidar Blueprint"))
  TSubclassOf<ALidar> lidar_class_;

  // ROS Stuff
  // Twist Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="Twist Subscriber Topic"))
  FString twist_sub_topic_name_;

  // Point Cloud Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="Point Cloud Publish Topic"))
  FString pt_cld2_pub_name_;

  // TF Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="TF Publish Topic"))
  FString tf2_pub_topic_name_;

  // GPS Odom Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="GPS odom Publish Topic"))
  FString gps_pub_name_;

  // Wheel Odom Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="Wheel odom Publish Topic"))
  FString wheel_pub_name_;

  // IMU Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="IMU Publish Topic"))
  FString imu_pub_topic_name_;

  // Sets the frequency for how often an IMU msg is sent over ROS (Hz).
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="IMU Publish Rate",
    ClampMin="0.0"))
  float imu_pub_rate_;

  // Sets the frequency for how often a GPS odometry msg is sent over ROS (Hz).
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Husky|ROS", meta=(DisplayName="GPS odom Publish Rate",
    ClampMin="0.0"))
  float gps_pub_rate_;

protected:
  // Expose this one to Blueprints so that the LiDAR can use it
  UPROPERTY(BlueprintReadOnly, Category="Husky", meta=(DisplayName="Initial Inverse Transform"))
  FTransform init_inv_trans_;

private:
  // Helper method for setting up the wheels
  void setupWheel(UStaticMeshComponent** wheel, UPhysicsConstraintComponent** phys, FName wheel_name, FName phys_name,
    FVector pos, UStaticMesh* mesh);

  // Callback functions for user input
  void addLengthToSA();
  void subLengthFromSA();
  void toggleCamera();

  // Callback functions for timeline events
  // They have to be UFUNCTIONs for this to work.
  UFUNCTION()
  void updateTorque();
  UFUNCTION()
  void handleOdomMsg();
  UFUNCTION()
  void clearTwstValues();

  // All the components
  // base_link will be RootComponent
  USpringArmComponent* spring_arm_;
  UCameraComponent* external_cam_, *on_board_cam_;
  UStaticMeshComponent* top_chassis_, *wheel_FL_, *wheel_FR_, *wheel_RL_, *wheel_RR_, *front_bumper_, *rear_bumper_;
  UStaticMeshComponent* top_plate_, *top_frame_, *vn_100_, *vlp_, *emlid_;
  UPhysicsConstraintComponent* wheel_phys_FL_, *wheel_phys_FR_, *wheel_phys_RL_, *wheel_phys_RR_;
  UChildActorComponent* vn_, *vlp_16_;

  // Our Timelines
  FTimeline cmd_vel_, clr_cmd_vel_, imu_pub_line_, gps_pub_line_;

  // ROS Stuff
  // Callback for ROS time publishing
  void pubClock(ELevelTick tick_type, float dt);

  // Callback for timelines for publishing imu and gps odom msgs
  UFUNCTION()
  void pubImuMsg();
  UFUNCTION()
  void pubGpsOdomMsg();

  // Topics for subscribers!
  // These need to be property'd so that they don't get garbage collected
  // https://github.com/code-iai/ROSIntegration/issues/32
  UPROPERTY()
  UTopic* twist_topic_;
  UPROPERTY()
  UTopic* imu_topic_;
  UPROPERTY()
  UTopic* tf2_topic_;
  UPROPERTY()
  UTopic* clock_topic_;
  UPROPERTY()
  UTopic* pt_cld2_topic_;
  UPROPERTY()
  UTopic* gps_topic_;
  UPROPERTY()
  UTopic* wheel_odom_topic_;

  // Other internal variables we use
  float engine_torque_;
  UPROPERTY()
  UCurveFloat* wheel_torque_curve_;
  float rpm_;
  bool is_accelerating_;
  FVector linear_, angular_, linear_V_, angular_V_;
  float old_time_, old_time_2_;
  // ROS Stuff
  UPROPERTY()
  FVector old_position_;
  UPROPERTY()
  FQuat old_orientation_;
};
