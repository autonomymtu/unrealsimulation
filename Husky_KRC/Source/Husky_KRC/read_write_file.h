/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */
#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "read_write_file.generated.h"

UCLASS()
class HUSKY_KRC_API UReadWriteFile : public UBlueprintFunctionLibrary
{
  GENERATED_BODY()
public:

  // Create the Blueprint callable buttons/stuff's
  UFUNCTION(BlueprintPure, Category="FileI/O", meta =(Keywords="Load Text"))
  static bool loadText(FString file_name, FString& save_text);

  UFUNCTION(BlueprintCallable, Category="FileI/O", meta=(Keywords="Save Text"))
  static bool saveText(FString save_text, FString file_name);
};
