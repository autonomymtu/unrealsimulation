# ROS/Unreal simulation project  
##	Purpose  
This simulation setup is a project based upon the Unreal game engine tied into ROS. The purpose is for better simulation as well as better real-time sensor performance.  
  
The ground mesh area/test area is the Keweenaw Research Center (KRC) off-road autonomy area.  
https://www.mtu.edu/krc/  
### Note: The entire mesh is not visualized in the editor currently but can easily be added. It is located in: Husky_KRC\Content\KRCCourse\*  
Just include any mesh piece you want and set the origin to 0,0,0  
    
##	Simulation requirements  
1. Make sure you have Unreal Engine version 4.20.3 installed. https://www.unrealengine.com/en-US/download  
2. Visual Studio 2017 also needs to be installed if on Windows: https://visualstudio.microsoft.com/downloads/  
3. Make sure you do a recursive clone in order to get the ROSIntegration Plugin or update after a clone with: ```git submodule update --init --recursive```  
  
## Launch/Run project  
1. Clone the project at: https://bitbucket.org/prometheusborealis/simulationunreal/src/master/  
2. This project contains the ROS integration plugin found at: https://github.com/code-iai/ROSIntegration  
When cloning the entire project make sure the directory at: simulationUnreal\Husky_KRC\Plugins\ROSIntegration is not empty.  
3. Go to simulationUnreal\Husky_KRC and launch project by double clicking Husky_KRC.uproject  
Note: For the first time if the project does not build:  
	1. right click on Husky_KRC.uproject  
	2. **Generate Visual Studio Project Files**  
	3. Double click Husky_KRC.sln  
	4. Build files in Visual Studio once launched  
  
4. On the same network open a different machine with Ubuntu 16.04 running  
5. Once the Unreal simulation is launched, navigate to Content/ROS_Communication  
6. Set the ROSBridge Server host according to the IP address of the Ubuntu machine   
![ROSBridge Server host](Documentation/gameInstance.JPG)  
7. Navigate to simulationUnreal\Ros_files\README.md for ROS instructions
  
## Coding style
For any changes try to follow the ROS style guidelines found below:  
1. http://wiki.ros.org/ROS/Patterns/Conventions  
2. http://wiki.ros.org/BestPractices  
3. http://wiki.ros.org/CppStyleGuide  

## Note: Unreal is not always as smart as it thinks. If you are experiencing an error and prior to setting an issue.  
Delete the following directories and then re-compile the project:  
1. simulationUnreal/Husky_KRC/Intermediate  
2. simulationUnreal/Husky_KRC/Saved  
3. simulationUnreal/Husky_KRC/Binaries  
4. simulationUnreal/Husky_KRC/Plugins/ROSIntegration/Binaries  
5. simulationUnreal/Husky_KRC/Plugins/ROSIntegration/Intermediate  
  
## Extra's  
1. To change any LiDAR parameter navigate to C++ instance of the LiDAR at Content\Lidar\Test  
![LiDAR parameters](Documentation/lidarParams.JPG)  
  
## Contact
```Sam Kysar  
skysar@mtu.edu```  
  
```Parker Young  
pjyoung@mtu.edu```  